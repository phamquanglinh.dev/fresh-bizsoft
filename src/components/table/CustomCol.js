import {Link} from "react-router-dom";

const CustomCol = ({value, style, teacher}) => {
    return (
        <td className={"bg-white shadow-lg p-2 px-3 border-[#dee2e6] border-[0.2px] min-w-[5rem]"} style={style}>
            {teacher != null ?
                <Link to={"/teacher/" + teacher.id + "/show"} className={"flex items-center justify-center"}>
                    <span
                        className={"w-[1.5rem] h-[1.5rem] rounded-full bg-blue-900 text-white text-sm mr-1 flex items-center justify-center"}>{value}</span>
                    <span className={"flex border p-1 rounded scale-[90%]"}>
                        <img src={teacher.avatar} className={"w-[1.5rem] rounded-full mr-2"}/>
                        <span className={"text-sm text-gray-600"}>{teacher.name}</span>
                    </span>
                </Link>
                : null}
        </td>
    )
}
export default CustomCol