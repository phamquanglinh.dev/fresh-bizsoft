import MainLayout from "../../layouts/MainLayout";
import Breadcrumbs from "../../components/Breadcrumbs";
import {useContext, useEffect, useState} from "react";
import TextInput from "../../components/inputs/TextInput";
import DateInput from "../../components/inputs/DateInput";
import FetchingAnimation from "../../components/FetchingAnimation";
import {Navigate, useNavigate, useParams} from "react-router-dom";
import appContext from "../../context/AppContext";
import TimeInput from "../../components/inputs/TimeInput";
import NumberInput from "../../components/inputs/NumberInput";
import AttendanceInput from "../../components/inputs/AttendanceInput";
import VideoRecordInput from "../../components/inputs/VideoRecordInput";
import Select from "react-select";
import SelectMultiInput from "../../components/inputs/SelectMultiInput";
import SelectInput from "../../components/inputs/SelectInput";

const LessonCreate = () => {
    const [user, setUser] = useState({})
    const app = useContext(appContext)
    const [label, setLabel] = useState("")
    const {id} = useParams()
    const {classroom_id} = useParams()
    useEffect(() => {
        if (id) {
            setLabel("Chỉnh sửa buổi học")
        } else {
            setLabel("Thêm buổi học mới")
        }
    }, [])
    const [init, setInit] = useState({})
    const [fetching, setFetching] = useState(false)
    const [data, setData] = useState({
        classroom_id: classroom_id,
        record: {
            type: "youtube"
        }
    })
    const uploadLesson = async () => {
        console.log(data)
        if (id) {
            await app.modifyLesson({
                postData: {
                    ...data,
                    id: id,
                }, setFetching: setFetching
            })
        } else {
            await app.modifyLesson({postData: {...data}, setFetching: setFetching})
        }
    }
    useEffect(() => {
        document.title = label
        app.setUserAction(setUser, setFetching).then()
    }, [])
    useEffect(() => {
        if (id) {
            setFetching(false)
            app.getLessonById({
                id: id,
                setData: setData,
                setFetching: setFetching
            }).then()

        }
        if (classroom_id) {
            app.initForLesson({
                classroom_id: classroom_id,
                init: init,
                setInit: setInit,
                setData: setData, data: data,
                setFetching: setFetching
            }).then()
        }
    }, [])
    return (
        <MainLayout>
            <div>
                <Breadcrumbs
                    parent={
                        {
                            label: "Danh sách buổi học",
                            link: "/lesson/list"
                        }
                    }
                    label={label}
                />
            </div>
            {!fetching ?
                <div>
                    <div className={"flex flex-wrap transition-all"}>
                        <div className={"lg:w-3/4 px-1 h-full mb-3"}>
                            <div className={"flex flex-wrap w-full border p-2 rounded bg-gray-50 pt-5 pb-20"}>
                                {classroom_id ? <div className={"md:w-1/5 w-full mb-3"}>
                                        <SelectInput label={"Giáo viên điểm danh"} options={init.initTeachers}
                                                     name={"teacher_id"}
                                                     placeholder={"Giáo viên"} data={data}
                                                     setData={setData}
                                                     nullable={true}
                                        />
                                    </div> :
                                    <div className={"md:w-1/5 w-full mb-3"}>
                                        <div className={"px-2"}>
                                            <label htmlFor={"teacher"}
                                                   className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Giáo
                                                viên điểm danh</label>
                                            <input
                                                className={"font-bold opacity-40 text-blue-900 rounded-lg bg-gray-50  border border-gray-300 text-sm block w-full p-2.5"}
                                                value={data.teacher ? data.teacher.name : ""}
                                                disabled={true}
                                            />
                                        </div>
                                    </div>
                                }
                                <div className={"md:w-1/5 w-full mb-3"}>
                                    <TextInput name={'session'} placeholder={init.initSession || 1}
                                               label={"Buổi học"}
                                               data={data}
                                               setData={setData}/>
                                </div>
                                <div className={"md:w-3/5 w-full mb-3"}>
                                    <TextInput name={'title'} placeholder={'Unit 1....'} label={"Tiêu đề bài học"}
                                               data={data}
                                               setData={setData}/>
                                </div>

                                <div className={"md:w-1/3 w-full mb-3"}>
                                    <DateInput name={'day'} label={"Ngày"} data={data}
                                               setData={setData}/>
                                </div>
                                <div className={"md:w-1/3 w-full mb-3"}>
                                    <TimeInput name={'start'} label={"Bắt đầu"} data={data}
                                               setData={setData}/>
                                </div>
                                <div className={"md:w-1/3 w-full mb-3"}>
                                    <TimeInput name={'end'} label={"Kết thúc"} data={data}
                                               setData={setData}/>
                                </div>
                                <div className={"md:basis-1/2 basis-full mb-3"}>
                                    <AttendanceInput
                                        name={'attendances'}
                                        data={data}
                                        setData={setData}
                                        label={'Điểm danh'}
                                        defaultValue={init.initStudents}
                                    />
                                </div>
                                <div className={"md:basis-1/2 basis-full "}>
                                    <VideoRecordInput
                                        name={'record'}
                                        data={data}
                                        setData={setData}
                                        label={'Video buổi học'}
                                    />
                                </div>
                                <div className={"md:w-1/2 w-full mb-3"}>
                                    <NumberInput name={'hour_salary'} label={"Lương theo giờ"} data={data}
                                                 setData={setData}
                                                 suffix={'đ'}
                                    />
                                </div>
                                <div className={"md:w-1/2 w-full mb-3"}>
                                    <TextInput name={'asm'} label={"Nhận xét chung"} data={data}
                                               setData={setData}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"w-full mb-3 px-2 mt-5"}>
                        {!id ? <button
                                onClick={uploadLesson}
                                className={"p-2 cursor-pointer hover:bg-blue-800 rounded bg-blue-900 text-white"}>Tạo buổi
                                học
                                mới
                            </button> :
                            <button
                                onClick={uploadLesson}
                                className={"p-2 cursor-pointer hover:bg-blue-800 rounded bg-blue-900 text-white"}>Chỉnh
                                sửa
                                buổi học
                            </button>
                        }
                    </div>
                </div> :
                <FetchingAnimation/>}

        </MainLayout>
    )
}
export default LessonCreate